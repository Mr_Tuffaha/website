<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="author" content="Omar Tuffaha">
    <meta name="description" content="Omar Tuffaha - this is my portfolio, This is my portfolio and here you can find all my skills and experience">
    <meta name="keywords" content="Omar Tuffaha, cv, portfolio">
    <meta property="og:image" content="">
    <meta property="og:description" content="This is my portfolio and here you can find all my skills and experience">
    <meta property="og:title" content="Omar Tuffaha">
    <title>Omar Tuffaha</title>

    <link href="https://use.fontawesome.com/releases/v5.0.7/css/all.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Karla:400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">

</head>
<body>
    <div class="nav" id="home">
        <div class="logo">
            <h2>Omar Tuffaha</h2>
        </div>
        <div class="nav-items">
            <a href="#home">Home</a>
            <a href="#about-me">About Me</a>
            <a href="#my-skills">Skills</a>
            <a href="#full-resume">Resume</a>
            <a href="#portfolio">Portfolio</a>
            <a href="#contact-me">Contact Me</a>
        </div>
    </div>
    <main>
        <div class="about-me" id="about-me">
            <div class="img_holder">
                <img src="img/profile_image.png" alt="">
            </div>
            <div class="details">
                <h3>ABOUT ME</h3>
                <p>I love to have new experiences and have new challenges, I love what ever makes me think. I am a person who is always willing to learn new things, also an open minded person specially on criticism (as long as its constructive criticism).</p>
                <p>I have been working with web development for the past 3 years along with some devops work. I believe I am really good with backend (<b>PHP</b>, <b>MYSQL</b>). I always devide my code to reusable blocks that are easy to debug and maintain most importantly readibility.</p>

                <div class="fields">
                    <div class="section">
                        <h4>Backend development</h4>
                        <i class="fab fa-php"></i>
                        <p>I have 3 years of professional experience and continue to develop my skills and I always learn security fixes to ensure all my projects are secure. I have experience working with Laravel framework with personal projects where I got good with this framework. Currently I am working with Laravel and improving my skills.</p>
                    </div>
                    <div class="section">
                        <i class="fas fa-database"></i>
                        <h4>Database Design</h4>
                        <p>I am really good with database design and planning. I always analize all aspects of a project before planning the database to ensure it always has what it needs.</p>
                    </div>
                    <div class="section">
                        <i class="fab fa-css3-alt"></i>
                        <h4>WEB DESIGN</h4>
                        <p>While this is not my strongest field as I focus on the backend, but I have developed my skills for html and css and javascript to copy and make website designs from easy to intermadiate psd designs and also I know very well how to connect my backend to the frontend.</p>
                    </div>
                    <div class="section">
                        <i class="fa fa-server"></i>
                        <h4>Bash and Linux server</h4>
                        <p>I am very comfortable in working with bash and do all the time build scripts to automate tasks and monitor precesses. Also I do know my way around the terminal and can setup, install required programs, set secure ssh connections, setup cronjobs and run bash scripts.</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="my-skills" id="my-skills">
            <h3>My skills</h3>
            <!-- <div class="skill-info">
                <h4>Kubernetes - good</h4>
                <p>I am currently in training to get the <a href="https://www.cncf.io/certification/ckad/">CKAD</a> certification and also I have some professional experience with it.</p>
            </div> -->
            <div class="skill-info">
                <h4>Laravel - really good</h4>
                <p>Currently working with laravel in my curent possition also used it on personal projects (currently with <b>Laravel-7</b>). I have really good understanding of most of the elements in laravel and can implemet most functionalities in a secure and organized way while keeping the code effecient and using the laravel helper functions.</p>
            </div>
            <div class="skill-info">
                <h4>PHP - Excelent</h4>
                <p>Work a lot with PHP (currently with <b>PHP-7.4</b>), especially making use of its OOP by making classes to every component of the website. I now work mostly with <b>Laravel</b> to be able to build better bigger projects faster and to use its libraries. Also learning and looking up conventions and security methods and holes that I need to learn and fix.</p>
            </div>
            <div class="skill-info">
                <h4>MariaDB - really good</h4>
                <p>Greate at planning mysql databases and build and use complex quiries, to not need to send multiple reqeausts to get some information. Use the mysql relations to put conditions on input and changes when editing entries, also make use of the <b>trigger</b> function to also send some of the work load to the database instead of having it all only to the backend.</p>
            </div>
            <div class="skill-info">
                <h4>Bash - really good</h4>
                <p>I am very comfortable to work with bash and build scripts to automate repititave tasks or to monitor processes.</p>
            </div>
            <div class="skill-info">
                <h4>html & css - good</h4>
                <p>Good enough to build necessary components and and basic designs, to also connect backendend to frontend and make them work togather. Also good at replicating web templates that are not too complex specially using the <b>CSS-GRID</b>. For example this page is designed of a ready psd template, with pure html and css with no frameworks.</p>
            </div>
            <div class="skill-info">
                <h4>javascript & jquery - good</h4>
                <p>Good enough to make input filters and munipulate the page to my needs, also working with ajax and some other libraries like <a href="https://datatables.net/" target="_blank">datatables</a>, <a href="http://html2canvas.hertzen.com/" target="_blank">html2canvas</a> and <a href="https://ckeditor.com" target="_blank">ckeditor</a>. When found upon a new problem I always try and search how can it be solved.</p>
            </div>
            <!-- <div class="skill-info">
                <h4>Magento - good</h4>
                <p>Currently working with Magento 2 in my current Job. But I already can build extentions and add functionality to it. Although after all of my experience with I have discoved that the Magento framework is not for me.</p>
            </div> -->
            <a href="#contact-me" class="contact-me-btn" name="button">Contact Me</a>
        </div>



        <div class="full-resume" id="full-resume">


            <div class="resume-section">
                <h3>work experience</h3>
                <div class="resume-list">
                    <div class="resume-item">
                        <h4>Laravel Backend developer</h4>
                        <h5><b><a href="https://www.antwerpes.com/" target="_blank">Antwerpes AG</a></b> / April 2021 - Current</h5>
                        <div class="line"></div>
                        <div class="point">
                            <i class="fas fa-circle"></i>
                            <p>Build new features and fix bugs in Laravel projects (Laravel, ddev,  LAMP-stack)</p>
                        </div>
                        <div class="point">
                            <i class="fas fa-circle"></i>
                            <p>Work on linux using automated ddev environment</p>
                        </div>
                        <div class="point">
                            <i class="fas fa-circle"></i>
                            <p>Work as part of Agile teams</p>
                        </div>
                    </div>
                    <div class="resume-item">
                        <h4>Maggento Backend developer</h4>
                        <h5><b><a href="https://www.sitewards.com/" target="_blank">Sitewards GmbH</a></b> / November 2019 - November 2020</h5>
                        <div class="line"></div>
                        <div class="point">
                            <i class="fas fa-circle"></i>
                            <p>Build new features and fix bugs in e-commerce projects (Magento, LAMP-stack)</p>
                        </div>
                        <div class="point">
                            <i class="fas fa-circle"></i>
                            <p>Work on linux using highly automated docker-based environment</p>
                        </div>
                        <div class="point">
                            <i class="fas fa-circle"></i>
                            <p>Fix issues on CI/CD pipelines that are working with AWS and Google Cloud, also involving Kubernetes</p>
                        </div>
                        <div class="point">
                            <i class="fas fa-circle"></i>
                            <p>Work as part of Agile teams</p>
                        </div>
                        <div class="point">
                            <i class="fas fa-circle"></i>
                            <p>Build a githook to validate git commits to follow company standards</p>
                        </div>
                    </div>
                    <div class="resume-item">
                        <h4>Web backend developer Internship</h4>
                        <h5><b><a href="https://www.roastmarket.de/" target="_blank">Roastmarket</a></b> / March 2019 - October 2019</h5>
                        <div class="line"></div>
                        <div class="point">
                            <i class="fas fa-circle"></i>
                            <p>Build new extentions for Magento</p>
                        </div>
                        <div class="point">
                            <i class="fas fa-circle"></i>
                            <p>Fix bugs on the site</p>
                        </div>
                        <div class="point">
                            <i class="fas fa-circle"></i>
                            <p>Add new functionality to the site</p>
                        </div>
                        <div class="point">
                            <i class="fas fa-circle"></i>
                            <p>Build bash scripts to monitor system processes and notify on failure</p>
                        </div>
                        <div class="point">
                            <i class="fas fa-circle"></i>
                            <p>Setup and monitor a replication database</p>
                        </div>
                        <div class="point">
                            <i class="fas fa-circle"></i>
                            <p>Build a stable docker image to be used by all future developers to ensure consistent work environment for development</p>
                        </div>
                        <div class="point">
                            <i class="fas fa-circle"></i>
                            <p>Give Tech support for other departments</p>
                        </div>
                    </div>
                    <div class="resume-item">
                        <h4>Web backend developer Internship</h4>
                        <h5><b><a href="http://www.superiors.jo/" target="_blank">Superiors ICT</a></b> / November 2016 - Februery 2017</h5>
                        <div class="line"></div>
                        <div class="point">
                            <i class="fas fa-circle"></i>
                            <p>Building web APIs to communicate with android applications</p>
                        </div>
                        <div class="point">
                            <i class="fas fa-circle"></i>
                            <p>Build and edited two databases needed for projects with mysql</p>
                        </div>
                        <div class="point">
                            <i class="fas fa-circle"></i>
                            <p>Build backend of a website</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="border-box">
                <div class="line"></div>
            </div> -->
            <div class="resume-section">
                <h3>My education</h3>
                <div class="resume-list">
                    <div class="resume-item">
                        <h4>Exchange semester</h4>
                        <h5><b><a href="https://www.th-brandenburg.de/" target="_blank">Brandenburg Technical University</a></b> / September 2018 - Februery 2019</h5>
                    </div>
                    <div class="resume-item">
                        <h4>BSC Computer Science</h4>
                        <h5><b><a href="http://www.gju.edu.jo/" target="_blank">German Jordanian University</a></b> / September 2014 - Augest 2019</h5>
                    </div>
                </div>
            </div>
            <!-- <div class="border-box">
                <div class="line"></div>
            </div> -->
            <div class="resume-section">
                <h3>Courses</h3>
                <div class="resume-list">
                    <div class="resume-item">
                        <h4>Online Kubernetes course</h4>
                        <h5><b><a href="https://www.udemy.com/course/certified-kubernetes-application-developer/" target="_blank">Udemy.com</a></b> / November 2020 - December 2020</h5>
                        <div class="line"></div>
                        <div class="point">
                            <i class="fas fa-circle"></i>
                            <p>Presently learning the laravel framework for PHP from udemy</p>
                        </div>
                        <div class="point">
                            <i class="fas fa-circle"></i>
                            <p>Making a full working project</p>
                        </div>
                    </div>
                    <div class="resume-item">
                        <h4>Online Laravel course</h4>
                        <h5><b><a href="https://www.udemy.com/php-with-laravel-for-beginners-become-a-master-in-laravel/" target="_blank">Udemy.com</a></b> / July 2018 - September 2018</h5>
                        <div class="line"></div>
                        <div class="point">
                            <i class="fas fa-circle"></i>
                            <p>Presently learning the laravel framework for PHP from udemy</p>
                        </div>
                        <div class="point">
                            <i class="fas fa-circle"></i>
                            <p>Making a full working project</p>
                        </div>
                    </div>
                    <div class="resume-item">
                        <h4>PHP Pearson Cirtification</h4>
                        <h5><b><a href="http://www.pioneers-academy.com" target="_blank">Pioneers Academy</a></b> / December 2015 - Februery 2016</h5>
                        <div class="line"></div>
                        <div class="point">
                            <i class="fas fa-circle"></i>
                            <p>PHP (web development) certified by Pearson through Pioneers Academy</p>
                        </div>
                    </div>
                    <div class="resume-item">
                        <h4>PHP Security concepts</h4>
                        <h5><b><a href="http://www.pioneers-academy.com" target="_blank">Pioneers Academy</a></b> / December 2015 - Februery 2016</h5>
                        <div class="line"></div>
                        <div class="point">
                            <i class="fas fa-circle"></i>
                            <p>Completed intensive 20 hours of PHP training on security concepts</p>
                        </div>
                    </div>
                    <div class="resume-item">
                        <h4>PHP Development</h4>
                        <h5><b><a href="http://www.pioneers-academy.com" target="_blank">Pioneers Academy</a></b> / December 2015 - Februery 2016</h5>
                        <div class="line"></div>
                        <div class="point">
                            <i class="fas fa-circle"></i>
                            <p>Completed intensive 80 hours of PHP training</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="box" id="resume">
            <div class="resume">
                <h3>resume</h3>
                <p>Download Full CV from here or continue to see my protfolio</p>
                <a target="_blank" href="OMAR_TUFFAHA_CV_GERMANY.pdf" class="download-btn">Resume</a>
                <p>Last updated at: 07.04.2021</p>

            </div>
        </div>
        <div class="portfolio" id="portfolio">
            <h3>portfolio</h3>
            <!-- <p>Here I have some of my achievments or projects that I built.</p> -->
            <div class="gallery">
                <a target="_blank" href="http://www.gju.edu.jo/news/gju-students-win-third-place-umniahs-tank-iot-hackathon-8700" class="gallery-box">
                    <img src="img/GJU_hackathon.jpg" alt="">
                    <div class="box-discription">
                        <p>Won third place in Umniah’s IOT hackathon, for a product idea to create IoT based E-Ink displays for electronic signage with a working demo.</p>
                    </div>
                </a>

                <a target="_blank" href="http://www.gju.edu.jo/news/gju-students-win-startup-weekend-jordan-9185" class="gallery-box">
                    <img src="img/start_up_weekend.jpg" alt="">
                    <div class="box-discription">
                        <p>Won second place in Startup weekend Jordan, for an idea that helps connect land owners with investors, also adding in a device that measures moisture land to help farmers reduce water needs and also to help investors choose good lands.</p>
                    </div>
                </a>
            </div>

        </div>

    </main>
    <footer>
        <div class="contact-me" id="contact-me">
            <h3>Contact me</h3>
            <div class="contact-info">
                <address>
                    <p>Email: <a href="mailto:omar_tuffaha@hotmail.com">Omar Tuffaha &ltomar_tuffaha@hotmail.com&gt</a></p>
                    <p>Country: Germany</p>
                </address>
            </div>
        </div>
        <div class="copywrite">
            <p id="copyright" property="dc:rights">&copy;
                <span property="dc:dateCopyrighted"><?php echo date('Y'); ?></span>
                <span property="dc:publisher">Omar Tuffaha</span>
            </p>
            <div class="social-media">
                <a target="_blank" href="#"><i class="fab fa-twitter-square"></i></a>
                <a target="_blank" href="#"><i class="fab fa-facebook-square"></i></a>
                <a target="_blank" href="https://gitlab.com/users/Mr_Tuffaha/projects"><i class="fab fa-gitlab"></i></a>
            </div>
        </div>
    </footer>
</body>
</html>
